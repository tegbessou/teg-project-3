#Rent a book

####First step : Setup project
- Setup docker (mariadb, nginx, phpfpm)
- Setup symfony (api platform, doctrine)
- Setup application react (react router, redux)
- Render app react by symfony

####Second step : form login
- Entity user (fosuser bundle)
- Réinitialisation de password
- JWT token API
- Session in react app

####Next step
....