#!/bin/bash

echo "Waiting for mysql"
until curl db:3306
do
  printf "."
  sleep 5
done

echo -e "\nmysql ready"