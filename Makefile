DOCKER_COMPOSE = docker-compose

## Down environment
down:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --remove-orphans --volumes
## Start containers (unpause)
build:
	@$(DOCKER_COMPOSE) build || true

## Start containers (unpause)
up:
	@$(DOCKER_COMPOSE) up -d

## Stop containers (pause)
stop:
	@$(DOCKER_COMPOSE) pause || true

# rules based on files
composer.lock: composer.json
	@echo composer.lock is not up to date.

## Run composer install
vendor: composer.json composer.lock
	$(DOCKER_COMPOSE_PHP) composer self-update
	$(DOCKER_COMPOSE_PHP) composer install

.env: .env.dist
	@if [ -f .env ]; \
	then\
		echo '\033[1;41m/!\ The .env.dist file has changed. Please check your .env file (this message will not be displayed again).\033[0m';\
		touch .env;\
		exit 1;\
	else\
		echo cp .env.dist .env;\
		cp .env.dist .env;\
	fi

## Reset
reset: down .env build up vendor db-reset data-reset

## Wait DB
wait-db:
	$(DOCKER_COMPOSE) exec -T -u www-data php sh /usr/local/bin/wait_db.sh

## Database
db-reset: wait-db
	@$(DOCKER_COMPOSE) exec php bin/console doctrine:database:drop --force --if-exists
	@$(DOCKER_COMPOSE) exec php bin/console doctrine:database:create
	@$(DOCKER_COMPOSE) exec php bin/console doctrine:migration:migrate --no-interaction

## Generate data
data-reset:
	@$(DOCKER_COMPOSE) exec php bin/console hautelook:fixtures:load --no-interaction
#################################
.DEFAULT_GOAL := help

# COLORS
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
RESET  := $(shell tput -Txterm sgr0)
TARGET_MAX_CHAR_NUM=30

help:
	@echo "${GREEN}BO Meyclub${RESET} https://bo.meyclub.docker"
	@echo "Micro Service: ${GREEN}$(MICRO_SERVICE_NAME)${RESET}"
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  ${YELLOW}%-$(TARGET_MAX_CHAR_NUM)s${RESET} ${GREEN}%s${RESET}\n", helpCommand, helpMessage; \
		} \
		isTopic = match(lastLine, /^###/); \
	    if (isTopic) { printf "\n%s\n", $$1; } \
	} { lastLine = $$0 }' $(MAKEFILE_LIST)