[1mdiff --git a/config/bootstrap.php b/config/bootstrap.php[m
[1mold mode 100644[m
[1mnew mode 100755[m
[1mdiff --git a/config/bundles.php b/config/bundles.php[m
[1mold mode 100644[m
[1mnew mode 100755[m
[1mindex 49d3fb6..fa680aa[m
[1m--- a/config/bundles.php[m
[1m+++ b/config/bundles.php[m
[36m@@ -2,4 +2,16 @@[m
 [m
 return [[m
     Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],[m
[32m+[m[32m    Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle::class => ['all' => true],[m
[32m+[m[32m    Symfony\Bundle\TwigBundle\TwigBundle::class => ['all' => true],[m
[32m+[m[32m    Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle::class => ['all' => true],[m
[32m+[m[32m    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],[m
[32m+[m[32m    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],[m
[32m+[m[32m    Symfony\Bundle\MakerBundle\MakerBundle::class => ['dev' => true],[m
[32m+[m[32m    Symfony\Bundle\SecurityBundle\SecurityBundle::class => ['all' => true],[m
[32m+[m[32m    Symfony\Bundle\WebProfilerBundle\WebProfilerBundle::class => ['dev' => true, 'test' => true],[m
[32m+[m[32m    Nelmio\Alice\Bridge\Symfony\NelmioAliceBundle::class => ['dev' => true, 'test' => true],[m
[32m+[m[32m    Fidry\AliceDataFixtures\Bridge\Symfony\FidryAliceDataFixturesBundle::class => ['dev' => true, 'test' => true],[m
[32m+[m[32m    Hautelook\AliceBundle\HautelookAliceBundle::class => ['dev' => true, 'test' => true],[m
[32m+[m[32m    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],[m
 ];[m
[1mdiff --git a/config/packages/cache.yaml b/config/packages/cache.yaml[m
[1mold mode 100644[m
[1mnew mode 100755[m
[1mdiff --git a/config/packages/dev/routing.yaml b/config/packages/dev/routing.yaml[m
[1mold mode 100644[m
[1mnew mode 100755[m
[1mdiff --git a/config/packages/framework.yaml b/config/packages/framework.yaml[m
[1mold mode 100644[m
[1mnew mode 100755[m
[1mindex 5e25aa4..e081435[m
[1m--- a/config/packages/framework.yaml[m
[1m+++ b/config/packages/framework.yaml[m
[36m@@ -1,4 +1,5 @@[m
 framework:[m
[32m+[m[32m    translator: { fallback: '%locale%' }[m
     secret: '%env(APP_SECRET)%'[m
     #csrf_protection: true[m
     #http_method_override: true[m
[1mdiff --git a/config/packages/routing.yaml b/config/packages/routing.yaml[m
[1mold mode 100644[m
[1mnew mode 100755[m
[1mindex a15c4ec..5c986a9[m
[1m--- a/config/packages/routing.yaml[m
[1m+++ b/config/packages/routing.yaml[m
[36m@@ -2,3 +2,4 @@[m [mframework:[m
     router:[m
         strict_requirements: ~[m
         utf8: true[m
[41m+[m
[1mdiff --git a/config/packages/test/framework.yaml b/config/packages/test/framework.yaml[m
[1mold mode 100644[m
[1mnew mode 100755[m
[1mdiff --git a/config/packages/test/routing.yaml b/config/packages/test/routing.yaml[m
[1mold mode 100644[m
[1mnew mode 100755[m
[1mdiff --git a/config/routes.yaml b/config/routes.yaml[m
[1mold mode 100644[m
[1mnew mode 100755[m
[1mdiff --git a/config/services.yaml b/config/services.yaml[m
[1mold mode 100644[m
[1mnew mode 100755[m
[1mindex 5c4b417..10bca7d[m
[1m--- a/config/services.yaml[m
[1m+++ b/config/services.yaml[m
[36m@@ -4,6 +4,7 @@[m
 # Put parameters here that don't need to change on each machine where the app is deployed[m
 # https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration[m
 parameters:[m
[32m+[m[32m    locale: 'en'[m
 [m
 services:[m
     # default configuration for services in *this* file[m
